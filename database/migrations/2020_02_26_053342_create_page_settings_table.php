<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('site_title');
            $table->text('tagline');
            $table->text('site_url');
            $table->text('email_address');
            $table->text('site_logo');
            $table->text('site_favicon');
            $table->longText('copyright_text');
            $table->text('permalink_seo')->nullable();
            $table->text('meta_keywords_seo');
            $table->text('meta_description_seo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_settings');
    }
}
