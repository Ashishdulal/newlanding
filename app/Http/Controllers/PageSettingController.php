<?php

namespace App\Http\Controllers;

use App\PageSetting;
use Illuminate\Http\Request;

class PageSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function show(PageSetting $pageSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(PageSetting $pageSetting)
    {
        $pagesetting = pageSetting::findorfail('1');
        return view ('backend.dashboard.page-setting.index',compact('pagesetting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageSetting $pageSetting,$id)
    {
        $page = pageSetting::findOrFail($id);
        $page->site_title = $request->site_title;
        $page->tagline = $request->tagline;
        $page->site_url = $request->site_url;
        $page->email_address = $request->email_address;
        $page->copyright_text = $request->copyright_text;
        $page->meta_keywords_seo = $request->meta_keywords_seo;
        $page->meta_description_seo = $request->meta_description_seo;
        if(file_exists($request->file('site_logo'))){
            $image = $request->file('site_logo');
            $imageName =  "home".time().'.'.$request->file('site_logo')->getClientOriginalName();
            $image->move(public_path('uploads/images'),$imageName);
            $page->site_logo = $imageName;
        }
        else{
            $page->site_logo = $page->site_logo;
        }
        if(file_exists($request->file('site_favicon'))){
            $image = $request->file('site_favicon');
            $imageName =  "favicon".time().'.'.$request->file('site_favicon')->getClientOriginalName();
            $image->move(public_path('uploads/images'),$imageName);
            $page->site_favicon = $imageName;
        }
        else{
            $page->site_favicon = $page->site_favicon;
        }
        $page->save();
        return redirect()->back()->with('success','Settings Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageSetting $pageSetting)
    {
        //
    }
}
