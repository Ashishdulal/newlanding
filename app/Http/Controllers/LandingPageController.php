<?php

namespace App\Http\Controllers;

use App\Subscriber;
use App\LandingPage;
use App\User;
use Auth;
use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = LandingPage::findOrFail('1');
        return view('backend.dashboard.landing.index',compact('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function subscriber()
    {
        $subscriber = Subscriber::latest()->paginate(10);
        return view ('backend.dashboard.subscribers.index',compact('subscriber'));
    }
    public function user()
    {
        $userInfo = Auth::user();
        return view ('backend.dashboard.user.index',compact('userInfo'));
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $page =LandingPage::findOrFail($id);
        if(file_exists($request->file('top_banner_image'))){
            $image = "top_banner_image".time().'.'.$request->file('top_banner_image')->getclientOriginalName();
            $location = public_path('uploads/images');
            $request->file('top_banner_image')->move($location, $image);
            $page->top_banner_image = $image;
        }
        else{
            $page->top_banner_image2 = $page->top_banner_image2;
        }
        if(file_exists($request->file('top_banner_image2'))){
            $image = "top_banner_image2".time().'.'.$request->file('top_banner_image2')->getclientOriginalName();
            $location = public_path('uploads/images');
            $request->file('top_banner_image2')->move($location, $image);
            $page->top_banner_image2 = $image;
        }
        else{
            $page->top_banner_image2 = $page->top_banner_image2;
        }
        $page->privacy_policy = $request->privacy_policy;
        $page->countdown_timer = $request->countdown_timer;
        if(file_exists($request->file('lower_left_banner'))){
            $image = "lower_left_banner".time().'.'.$request->file('lower_left_banner')->getclientOriginalName();
            $location = public_path('uploads/images');
            $request->file('lower_left_banner')->move($location, $image);
            $page->lower_left_banner = $image;
        }
        else{
            $page->lower_left_banner = $page->lower_left_banner;
        }
        if(file_exists($request->file('lower_right_banner'))){
            $image = "lower_right_banner".time().'.'.$request->file('lower_right_banner')->getclientOriginalName();
            $location = public_path('uploads/images');
            $request->file('lower_right_banner')->move($location, $image);
            $page->lower_right_banner = $image;
        }
        else{
            $page->lower_right_banner = $page->lower_right_banner;
        }
        $page->save();
        return redirect()->back()->with('success','Updated Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LandingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function show(LandingPage $landingPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LandingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function edit(LandingPage $landingPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LandingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LandingPage $landingPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LandingPage  $landingPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(LandingPage $landingPage)
    {
        //
    }
}
