@extends('layouts.backend.app')

@section('content')
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Landing Page</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="/home">Dashboard</a></li>
					<li>
						Landing Page
					</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if ($message = Session::get('success'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>{{ $message }}</strong>
		</div>
		@endif
		<div class="card-body card-block white-box">
			<span class="au-breadcrumb-span note-space">[Note: <a href="#help_image">Click For Help</a>]</span>
			<form action="/home/landingpage/{{$page->id}}" method="post" class="form-horizontal form-material" enctype="multipart/form-data">
				@csrf
				
				<button style="float: right;" type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
				<br>
				<br>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="top_banner_image" class=" form-control-label"> Banner Image (2)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 1700*690 and should be less than 500kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" onchange="readURL(this);" id="top_banner_image" accept="image/png, image/jpg, image/jpeg" name="top_banner_image" class="form-control-file" data-id="banner-img1"><br>
						<img id="banner-img1" src="/uploads/images/{{$page->top_banner_image}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="top_banner_image" class=" form-control-label">Responsive Banner Image (2)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 1000*600 and should be less than 500kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" onchange="readURL(this);" id="top_banner_image2" accept="image/png, image/jpg, image/jpeg" name="top_banner_image2" class="form-control-file" data-id="banner-img1"><br>
						<img id="banner-img1" src="/uploads/images/{{$page->top_banner_image2}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="countdown_timer" class=" form-control-label">Countdown Timer(3)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="date" id="countdown_timer" name="countdown_timer" value="{{$page->countdown_timer}}" class="form-control">

					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="lower_left_banner" class=" form-control-label"> Lower Left Banner Image (5)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 626*365 and should be less than 500kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" onchange="readURL(this);" id="lower_left_banner" accept="image/png, image/jpg, image/jpeg" name="lower_left_banner" class="form-control-file" data-id="lower-banner"><br>
						<img id="lower-banner" src="/uploads/images/{{$page->lower_left_banner}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="lower_right_banner" class=" form-control-label">Lower Right Banner Image (6)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size 626*365 and should be less than 500kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input type="file" onchange="readURL(this);" id="lower_right_banner" accept="image/png, image/jpg, image/jpeg" name="lower_right_banner" class="form-control-file" data-id="lower-right-banner"><br>
						<img id="lower-right-banner" src="/uploads/images/{{$page->lower_right_banner}}" alt="Image">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="privacy_policy" class=" form-control-label">Privacy Policy (7)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="privacy_policy"  rows="9"  class="form-control ckeditor">{{$page->privacy_policy}}</textarea>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</form><br><br>
		</div>
		<div id="help_image" class="card-footer">
			<span class="au-breadcrumb-span">[The below Numbers in the image denotes upper number for changes.]  </span><br><br>
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">
						<img src="/uploads/images/newlanding-details.jpg" alt="Image">
					</div>
				</div>
				<style type="text/css">
					.help-image img {
						max-width: 100%;
					}
					.help-image img:hover {
						transform: scale(1.1);
					}
					img#lower-banner,img#lower-right-banner,img#banner-img1 {
						max-width: 100%;
					}
				</style>
				@endsection