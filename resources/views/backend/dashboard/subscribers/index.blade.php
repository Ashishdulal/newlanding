@extends('layouts.backend.app')

@section('content')
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Registered</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="/home">Dashboard</a></li>
					<li>
						Registered
					</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="white-box">
			<h3 class="box-title">Recent Registered</h3>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>PHONE NUMBER</th>
							<th>DATE</th>
						</tr>
					</thead>
					<tbody>
						@foreach($subscriber as $subs)
						<tr>
							<td>{{$loop->iteration}}</td>
							<td class="txt-oflo">{{$subs->mobile_number}}</td>
							<td class="txt-oflo">{{$subs->created_at->format('d M , Y')}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="text-center"> 
					{{$subscriber->links()}}
				</div>
			</div>
		</div>
	</div>
	@endsection