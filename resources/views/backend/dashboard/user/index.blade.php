@extends('layouts.backend.app')

@section('content')
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User Profile</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/home">Dashboard</a></li>
                    <li>
                       User Profile
                   </li>
               </ol>
           </div>
           <!-- /.col-lg-12 -->
       </div>
       @if (count($errors) > 0)
       <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if ($message = Session::get('status'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="white-box new-user">
        <a href="/register" class="btn btn-primary">Register New User</a>
    </div>
    <!-- .row -->
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg"> <img width="100%" alt="user" src="/uploads/images/admin-image.png">
                    <div class="overlay-box">
                        <div class="user-content">
                            <a href="javascript:void(0)"><img src="/uploads/images/admin-image.png"
                                class="thumb-lg img-circle" alt="img"></a>
                                <h4 class="text-white">{{$userInfo->name}}</h4>
                                <h5 class="text-white">{{$userInfo->email}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="user-btm-box text-center">
                        <a href="/password/reset" class="btn btn-primary">Reset Password</a>

                    </div>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="white-box">
                    <form class="form-horizontal form-material">
                        <div class="form-group">
                            <label class="col-md-12">Name</label>
                            <div class="col-md-12">
                                <input type="text" value="{{$userInfo->name}}"
                                class="form-control form-control-line" readonly> </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" value="{{$userInfo->email}}"
                                    class="form-control form-control-line" name="example-email"
                                    id="example-email" readonly> </div>
                                </div>
<!--                                 <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" placeholder="Password" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div> -->
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            @endsection