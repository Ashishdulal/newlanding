<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\LandingPage;
use Carbon\Carbon;

class LandingPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$page = new LandingPage();
        $page->top_banner_image = 'slider1.png';
    	$page->top_banner_image2 = 'slider2.png';
    	$page->privacy_policy = 'A PRIVACY POLICY IS A DOCUMENT WHERE YOU DISCLOSE WHAT PERSONAL DATA YOU COLLECT FROM YOUR WEBSITE’S VISITORS, HOW YOU COLLECT IT, HOW YOU USE IT AND OTHER IMPORTANT DETAILS ABOUT YOUR PRIVACY PRACTICES.IN THIS POST, WE’LL TAKE A LOOK AT WHAT PRIVACY POLICIES ARE AND WHY YOU LIKELY NEED TO HAVE ONE POSTED ON YOUR WEBSITE. WE’LL ALSO GO OVER SOME IMPORTANT CLAUSES THAT ARE USEFUL TO INCLUDE IN YOUR PRIVACY POLICY. FINALLY, WE’LL LOOK AT HOW DIFFERENT WEBSITES DISPLAY THEIR PRIVACY POLICIES.';
    	$page->countdown_timer = Carbon::now();
    	$page->lower_left_banner = 'background8.jpg';
    	$page->lower_right_banner = 'background7.jpg';
    	$page->save();
    }
}