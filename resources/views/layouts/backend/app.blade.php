<!DOCTYPE html>
<html lang="en">
<head>
  <?php 
use App\PageSetting;
      $pageSetting = PageSetting::findOrFail('1');
  ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/uploads/images/{{$pageSetting->site_favicon}}">
    <title>{{$pageSetting->site_title}} | Dashboard</title>
    <!-- Bootstrap Core CSS -->
    <link href="/styles/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/styles/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="/styles/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="/styles/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="/styles/plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="/styles/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="/styles/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/styles/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="/styles/css/colors/default.css" id="theme" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

    @yield('setting')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        @include('layouts.backend.sidebar')
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            @yield('content')
            <!-- /.container-fluid -->
            <footer class="footer text-center"> <?php echo ($pageSetting->copyright_text)?> </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="/styles/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/styles/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="/styles/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="/styles/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="/styles/js/waves.js"></script>
    <!--Counter js -->
    <script src="/styles/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="/styles/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!-- chartist chart -->
    <script src="/styles/plugins/bower_components/chartist-js/dist/chartist.min.js"></script>
    <script src="/styles/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="/styles/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="/styles/js/custom.min.js"></script>
    <script src="/styles/js/dashboard1.js"></script>
    <script src="/styles/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script>
        CKEDITOR.replaceClass('ckeditor');

    </script>
    <script type="text/javascript">
     function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var id = $(input).attr('data-id');
            reader.onload = function (e) {
                $('#' + id).attr('src', e.target.result)
                .width('50%')
                .height('90%');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(document).on("change", ".file_multi_video", function(evt) {
      var $source = $('#video_here');
      $source[0].src = URL.createObjectURL(this.files[0]);
      $source.parent()[0].load();
  });
</script>
</body>

</html>
