<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{
    protected $fillable=['logo','top_banner_image','top_banner_image2','email_sender','privacy_policy','countdown_timer',
'lower_left_banner','lower_right_banner','copyright'];
}
