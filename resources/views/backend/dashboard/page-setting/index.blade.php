@extends('layouts.backend.app')
@section('setting')
<style type="text/css">
	.bg-title {
    padding: 30px 10px 9px;
}
	.top-left-part a {
    line-height: 80px;
}
	.logo b {
		float: inherit !important;
		padding-left: 0px !important;
	}
	a.nav-toggler.open-close.waves-effect.waves-light.hidden-md.hidden-lg {
    top: 20;
}
	.navbar-sidebar2 .navbar__list li .arrow {
		top: auto !important;
		height: 20px !important;
	}
	.btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .open>.dropdown-toggle.btn-default {
		color: #333;
		background-color: #e6e6e6;
		border-color: #adadad;
	}
	.btn-default {
		color: #333;
		background-color: #fff;
		border-color: #ccc !important;
	}
	.toggle-handle {
		position: relative;
		margin: 0 auto;
		padding-top: 0px;
		padding-bottom: 0px;
		height: 100%;
		width: 0px;
		border-width: 0 1px;
	}
	.nav-tabs>li {
		float: left;
		margin-bottom: -1px;
	}

	.nav>li {
		position: relative;
		display: block;
	}
	.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
		color: #555;
		cursor: default;
		background-color: #fff;
		border: 1px solid #ddd;
		border-bottom-color: transparent;
	}
	.nav-tabs>li>a {
		margin-right: 2px;
		line-height: 1.42857143;
		border: 1px solid transparent;
		border-radius: 4px 4px 0 0;
	}
	.nav>li>a {
		position: relative;
		display: block;
		padding: 10px 15px;
	}
	a:hover, a {
		-webkit-transition: all 0.3s ease;
		-o-transition: all 0.3s ease;
		-moz-transition: all 0.3s ease;
		transition: all 0.3s ease;
	}
	.tab-content {
    border: 1px solid #ddd !important;
    border-top: none !important;
    padding: 20px;
}
.nav-tabs {
    border-bottom: 1px solid #dee2e6;
}
</style>
@endsection
@section('content')
<!-- PAGE CONTAINER-->
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Landing Page</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="/home">Dashboard</a></li>
					<li>
						Landing Page
					</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if ($message = Session::get('success'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>{{ $message }}</strong>
		</div>
		@endif
		@if($pagesetting)
		<div class="white-box">
			<form action="/home/setting/{{$pagesetting->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<div style="text-align: right;margin: 20px 40px 0 0px;" class="home-btn">
					<button type="submit" class="btn btn-primary btn-sm">
						<i class="fa fa-dot-circle-o"></i> Update
					</button>
				</div>
				<div class="card-body card-block">
					<div class="container-fluid">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">General</a></li>
							<li><a data-toggle="tab" href="#menu1">Footer</a></li>
							<li><a data-toggle="tab" href="#menu2">SEO</a></li>
							<li></li>
						</ul>
						<div class="tab-content">
							<div id="home" class="tab-pane fade active in">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="site_title" class=" form-control-label">Site Title</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="site_title"  rows="9"  class="form-control" value="{{$pagesetting->site_title}}" placeholder="site_title"><br>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="tagline" class=" form-control-label">Tagline</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="tagline"  rows="9"  class="form-control" value="{{$pagesetting->tagline}}" placeholder="tagline"><br>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="site_url" class=" form-control-label">Site Url</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="site_url"  rows="9"  class="form-control" value="{{$pagesetting->site_url}}" placeholder="site url"><br>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="email_address" class=" form-control-label">Email Address</label><br>
										<span class="au-breadcrumb-span">[ Note: This will be the email address to recieve all the mails sent from the website. ]</span>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="email_address"  rows="9"  class="form-control" value="{{$pagesetting->email_address}}" placeholder="email address"><br>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="site_logo" class=" form-control-label">Site Logo</label><br>
										<span class="au-breadcrumb-span">[ Note: Please upload the small image size of '.png' type of image size 150*75 & should be less than 100kb ]</span>
									</div>
									<div class="col-12 col-md-9 process">
										<input onchange="readURL(this);" type="file" id="site_logo" accept="image/png, image/jpg, image/jpeg" name="site_logo" class="form-control-file" data-id="logo_upload"><br>
										<img id="logo_upload" src="/uploads/images/{{$pagesetting->site_logo}}" alt="site Logo">
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="site_favicon" class=" form-control-label">Favicon</label><br>
										<span class="au-breadcrumb-span">[ Note: Please upload the small image size of '.png' type & should be less than 10kb ]</span>
									</div>
									<div class="col-12 col-md-9">
										<input onchange="readURL(this);" type="file" id="site_favicon" accept="image/png, image/jpg, image/jpeg" name="site_favicon" class="form-control-file" data-id="fav_icon"><br>
										<img id="fav_icon" src="/uploads/images/{{$pagesetting->site_favicon}}" alt="site Favicon">
									</div>
								</div>
							</div>
							<div id="menu1" class="tab-pane fade">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="copyright_text" class=" form-control-label">Copyright</label>
									</div>
									<div class="col-12 col-md-9">
										<textarea name="copyright_text"  rows="9"  class="form-control ckeditor">{{$pagesetting->copyright_text}}</textarea>
									</div>
								</div>
							</div>
							<div id="menu2" class="tab-pane fade">
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="meta_keywords_seo" class=" form-control-label">Meta Keywords</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="meta_keywords_seo"  rows="9"  class="form-control" value="{{$pagesetting->meta_keywords_seo}}" placeholder="meta keywords for seo"><br>
									</div>
								</div>
								<div class="row form-group">
									<div class="col col-md-3">
										<label for="meta_description_seo" class=" form-control-label">Meta Description</label>
									</div>
									<div class="col-12 col-md-9">
										<input type="text" name="meta_description_seo"  rows="9"  class="form-control" value="{{$pagesetting->meta_description_seo}}" placeholder="meta description for seo"><br>
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-sm">
								<i class="fa fa-dot-circle-o"></i> Update
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		@endif
		<div class="card-footer">
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">

					</div>
				</div>
			</div>
		</div>
	</div><!--/.col-->
	@endsection