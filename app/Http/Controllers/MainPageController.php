<?php

namespace App\Http\Controllers;

use App\LandingPage;
use App\PageSetting;
use App\Subscriber;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    public function homePage(){
    	$page = LandingPage::findOrFail('1');
    	$totalCount = Subscriber::count();
        $pageSetting = PageSetting::findOrFail('1');
    	return view('frontend.index',compact('page','totalCount','pageSetting'));
    }
    public function registerPhn(Request $request){
    	$this->validate($request,[
            'full_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            ]);
    	$subs = new Subscriber();
    	$subs->mobile_number = $request->full_number;
    	$subs->save();
    	return redirect()->back()->with('success','Registration is completed.');
    }
}
