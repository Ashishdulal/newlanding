<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','MainPageController@homePage');

Route::post('/registered', 'MainPageController@registerPhn');

Route::fallback(function() {
    return view('nopage');
});

Route::get('/home/landing', 'LandingPageController@index');
Route::get('/home/users', 'LandingPageController@user');
Route::get('/home/setting', 'PageSettingController@edit');
Route::post('/home/setting/{id}', 'PageSettingController@update');
Route::get('/home/subscribers/all', 'LandingPageController@subscriber');
Route::post('/home/landingpage/{id}', 'LandingPageController@store');