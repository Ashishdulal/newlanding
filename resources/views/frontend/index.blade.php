@extends('layouts.frontend.app', ['title' => 'Landing Page'],['discription'=> ($pageSetting->tagline)])
@section('metaDescription')
<meta name="tagline" content="{{$pageSetting->tagline}}">
<meta name="description" content="{{$pageSetting->meta_description_seo}}">
<meta name="site url" content="{{$pageSetting->site_url}}">
<meta name="keywords" content="{{$pageSetting->meta_keywords_seo}}">
@endsection
@section('bannerImage')
<style type="text/css">
	@media only screen and (max-width: 430px){
		section.first-slider-part {
			background: url(/uploads/images/{{$page->top_banner_image2}}) !important;
			background-size: contain !important;
			background-repeat: no-repeat !important;
		}
	</style>
	@endsection
	@section('content')
	<section class="first-slider-part" style="background: url(/uploads/images/{{$page->top_banner_image}});">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-7 first-part-div">
					<img src="">
<!-- 				<div class="gift-info">
					<div class="row">
						<div class="col-sm-4 award-img-div">
							<img class="award-img" src="/uploads/images/gift-box.png">
						</div>
						<div class="col-sm-8">
							<div class="award-details"><br>
								<div class="top-award-details">
									<h1>Maplestory M</h1>
									<h2>PRE-REGISTRATION AWARD</h2>
								</div>
								<div class="lower-award-details">
									<ul>
										<li>SKY BICYCLE</li>
										<li>KITTI WORD BUBBLE RING</li>
										<li>CAT-GER LABEL RING</li>
										<li>NORMAL HAIR COUPON</li>
										<li>NORMAL PLASTIC SURGERY COUPEN</li>
										<li>TELEPORT ROCK X 10</li>
										<li>RESPAWN TOKEN X 10</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="lower-award-part">
						<div class="row">
							<div class="col-sm-4 lower-side-details">
								<div class="lower-image">
									<img src="/uploads/images/small-gift-box1.png">
									<p>To 500k Menbers<br>50cr</p>
								</div>
							</div>
							<div class="col-sm-4 lower-side-details">
								<div class="lower-image">
									<img src="/uploads/images/small-gift-box1.png">
									<p>To 1000k Menbers<br>150cr<br>gift vouchers</p>
								</div>
							</div>
							<div class="col-sm-4 lower-side-details">
								<div class="lower-image">
									<img src="/uploads/images/small-gift-box1.png">
									<p>To 2000k Menbers<br>3000cr<br>gift vouchers</p>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
			<div class="col-sm-5 second-part-div">
				<div class="contact-part-background">
					<div class="right-contact-part">
					<!-- <h1>Pre-Register</h1>
						<p>Every User will receive reward<br> just by pre-registering upto the specified amount !!</p> -->
						<div class="landing-form-divison">
							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert">×</button>
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							@if ($message = Session::get('success'))

							<!-- Modal -->
							<div class="modal fade" id="myMessage" role="dialog">
								<div style="margin: 9rem auto;" class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<h4 style="color: black;" class="modal-title"><strong>{{ $message }}</strong></h4>
											<button style="padding: 1.5rem 1rem;" type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
									</div>

								</div>
							</div>
							<script type="text/javascript">
								$('#myMessage').modal();
							</script>
							@endif
							<form action="/registered" method="post" class="landing-form">
								@csrf
								<div class="form-group">
									<label for="phoneNumber">Please enter your phone number, no country code or spaces are required.</label>
									<input type="tel" class="form-control" id="phoneNumber" pattern="[0-9]{10}" name="mobile_number" aria-describedby="phoneHelp" required>
									<!-- <small id="phoneHelp" class="form-text text-muted">We'll never share your phone number with anyone else.</small> -->
								</div>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="exampleCheck1" required >
									<label class="form-check-label" for="exampleCheck1" >I have read and agree to <a href="#" data-toggle="modal" data-target="#privacyPolicy">Privacy Policy</a>.</label>
									<!-- Modal -->
									<div class="modal fade" id="privacyPolicy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Privacy Policy</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div style="text-align: left;" class="modal-body">
													<?php echo ($page->privacy_policy)?>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div><br>
								<button type="submit" class="btn btn-primary">Registration confirmation</button>
							</form>
						</div>
					</div>
					<div class="Lower-count-users">
						<h2>{{$totalCount}}</h2>
						<p><b>Users have already registered.</b><br></p>
						<!-- <span class="lower-text-users">For the rewards in pre-registeration and like events for each level ,one account will only receive rewards one time per level.</span></p> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="landing-countdown-timer">
		<div class="container">
			<ul class="counter-display">
				<li class="countdown-timer-box"><span id="days"></span>days</li>
				<li class="countdown-timer-box"><span id="hours"></span>Hours</li>
				<li class="countdown-timer-box"><span id="minutes"></span>Minutes</li>
				<li class="countdown-timer-box"><span id="seconds"></span>Seconds</li>
			</ul>
		</div>
	</section>
	<section class="first-slider-part">
		<div class="container-fluid">
			<div class="row lower-img-part">
				<div class="col-sm-6 landing-desc-left">
					<img src="/uploads/images/{{$page->lower_left_banner}}" class="lower-landing-image">

<!-- 			<div class="landing-bottom-desc">
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div> -->
		</div>
		<div class="col-sm-6 landing-desc-left">
			<img src="/uploads/images/{{$page->lower_right_banner}}" class="lower-landing-image">
<!-- 			<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
  
			<div class="carousel-inner">
				<div class="item carousel-item active">
					<div class="row">
						<div class="col-sm-4">
								<div class="img-lower">
									<img src="/uploads/images/Screenshot_1.jpg" class="lower-responsive img-fluid" alt="">
								</div>					
							</div>
							<div class="col-sm-4">
								<div class="img-lower">
									<img src="/uploads/images/Screenshot_2.jpg" class="lower-responsive img-fluid" alt="">
								</div>					
							</div>
							<div class="col-sm-4">
								<div class="img-lower">
									<img src="/uploads/images/Screenshot_3.jpg" class="lower-responsive img-fluid" alt="">
								</div>					
							</div>
						</div>
					</div>
	<div class="item carousel-item">
					<div class="row">
							<div class="col-sm-4">
								<div class="img-lower">
									<img src="/uploads/images/Screenshot_1.jpg" class="lower-responsive img-fluid" alt="">
								</div>					
							</div>

					</div>
				</div>
			</div>

			<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div> -->
	</div>
</div>
</div>
</section>
<script src="{{asset('/js/intlTelInput.js')}}"></script>
<script>
	var input = document.querySelector("#phoneNumber");
	window.intlTelInput(input, {
		initialCountry: "auto",
		preferredCountries: [],
		hiddenInput: "full_number",
		geoIpLookup: function(callback) {
			$.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
  utilsScript: "{{asset('/js/utils.js?1562189064761')}}" // just for formatting/placeholders etc
});
</script>
<input type="hidden" id="counter_date" value="{{$page->countdown_timer}}">
<script type="text/javascript">
	const second = 1000,
	minute = second * 60,
	hour = minute * 60,
	day = hour * 24;
	var time = document.getElementById("counter_date").value;
	var dateTime = new Date(time);
	let countDown = dateTime.getTime(),
	x = setInterval(function() {

		let now = new Date().getTime(),
		distance = countDown - now;
		if(distance <= 0){
			document.getElementById('days').innerText = 0,
			document.getElementById('hours').innerText = 0,
			document.getElementById('minutes').innerText = 0,
			document.getElementById('seconds').innerText = 0;
		}
		else{
			document.getElementById('days').innerText = Math.floor(distance / (day)),
			document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
			document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
			document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
		}
      //do something later when date is reached
      //if (distance < 0) {
      //  clearInterval(x);
      //  'IT'S MY BIRTHDAY!;
      //}

  }, second)
</script>
@endsection