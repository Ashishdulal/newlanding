<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('top_banner_image');
            $table->text('top_banner_image2');
            $table->text('privacy_policy');
            $table->date('countdown_timer');
            $table->text('lower_left_banner');
            $table->text('lower_right_banner');
            $table->text('copyright')->nullable();
            $table->text('otp_api_key')->nullable();
            $table->text('otp_secret_key')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_pages');
    }
}
